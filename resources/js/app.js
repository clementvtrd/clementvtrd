import Swup from 'swup';

const swup = new Swup({
    containers: ["main", "nav"]
});

function progress() {
    $('#selenium').progress({
        label: 'ratio',
        text: {
            ratio: '{value}/{total}'
        }
    });
}

swup.on('contentReplaced', progress);
$(progress)
