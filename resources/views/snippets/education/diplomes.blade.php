<div class="ui grid">
    <div class="row">
        <article class="seven wide column ui container segment">
            <p class="ui blue ribbon label">En cours</p>
            <span>Master informatique <abbr title="Internet, Données et Connaissances">IDC</abbr></span>
            <section class="ui container">
                <div class="ui list">
                    <div class="item"></div>
                    <div class="item">
                        <i class="graduation cap brown icon"></i>
                        <div class="content">
                            <div class="header">Deuxième année</div>
                            <div class="description">spécialisation</div>
                            <div class="list">
                                <div class="item">
                                    <i class="cogs brown icon"></i>
                                    <div class="content">
                                        <div class="header">Web sémantique</div>
                                        <div class="description">définition et usage des ontologies</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="cog brown icon"></i>
                                    <div class="content">
                                        <div class="header">Connaissances et Raisonnement</div>
                                        <div class="description">création d'une ontologie</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="chart bar brown icon"></i>
                                    <div class="content">
                                        <div class="header">IHM - visualisation de données</div>
                                        <div class="description">initiation à l'UX design</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="shield brown icon"></i>
                                    <div class="content">
                                        <div class="header">Serveur des données et sécurité des applications</div>
                                        <div class="description">gestion et sécurisation d'un serveur</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="search brown icon"></i>
                                    <div class="content">
                                        <div class="header">Moteur de recherche</div>
                                        <div class="description">solution pour indexer et rechercher des données</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="share alternate brown icon"></i>
                                    <div class="content">
                                        <div class="header">Web analitique</div>
                                        <div class="description">analyse de graphes de réseau</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="graduation cap brown icon"></i>
                        <div class="content">
                            <div class="header">Première année</div>
                            <div class="description">tronc commun</div>
                            <div class="list">
                                <div class="item">
                                    <i class="sitemap brown icon"></i>
                                    <div class="content">
                                        <div class="header">Système et réseau</div>
                                        <div class="description">initiation à la gestion d'un réseau d'entreprise</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="code brown icon"></i>
                                    <div class="content">
                                        <div class="header">Programmation d'application web</div>
                                        <div class="description">généralisation sur les frameworks</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="object group outline brown icon"></i>
                                    <div class="content">
                                        <div class="header">Patrons de conception avancés</div>
                                        <div class="description">réflexion sur les design pattern</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="language brown icon"></i>
                                    <div class="content">
                                        <div class="header">Ingénierie des langues</div>
                                        <div class="description">initiation au NLP</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="database brown icon"></i>
                                    <div class="content">
                                        <div class="header">Base de données non traditionnelle</div>
                                        <div class="description">étude des bases NOSQL</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="chess brown icon"></i>
                                    <div class="content">
                                        <div class="header">Complexité et calculabilité</div>
                                        <div class="description">étude de problème (non) réalisable</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="download brown icon"></i>
                                    <div class="content">
                                        <div class="header">Knowledge acquisition</div>
                                        <div class="description">initiation aux algorithmes d'extraction de connaissances</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="file alternate outline brown icon"></i>
                                    <div class="content">
                                        <div class="header">Algorithme du texte</div>
                                        <div class="description">indexation et recherche plein texte</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
        <article class="seven wide column ui container segment">
            <p class="ui olive ribbon label">2015-2019</p>
            <span>Licence informatique</span>
            <section class="ui container">
                <div class="ui list">
                    <div class="item"></div>
                    <div class="item">
                        <i class="graduation cap brown icon"></i>
                        <div class="content">
                            <div class="header">Troisième année</div>
                            <div class="list">
                                <div class="item">
                                    <i class="object group outline brown icon"></i>
                                    <div class="content">
                                        <div class="header">Méthode et conception</div>
                                        <div class="description">initiation aux design pattern communs</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="coffee brown icon"></i>
                                    <div class="content">
                                        <div class="header">Conduite de projet</div>
                                        <div class="description">organisation et gestion d'un projet informatique</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="linux brown icon"></i>
                                    <div class="content">
                                        <div class="header">Système</div>
                                        <div class="description">prise en main du système UNIX</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="chess rook brown icon"></i>
                                    <div class="content">
                                        <div class="header">Aide à la décision et intelligence artificielle</div>
                                        <div class="description">découverte d'un domaine informatique</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="graduation cap brown icon"></i>
                        <div class="content">
                            <div class="header">Deuxième année</div>
                            <div class="list">
                                <div class="item">
                                    <i class="file code outline brown icon"></i>
                                    <div class="content">
                                        <div class="header">Introduction à la programmation orientée objet</div>
                                        <div class="description">initiation aux classes et à l'héritage</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="edit outline brown icon"></i>
                                    <div class="content">
                                        <div class="header">Algorithmique</div>
                                        <div class="description">étude des principaux algorithmes</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="superscript brown icon"></i>
                                    <div class="content">
                                        <div class="header">Calcul scientifique</div>
                                        <div class="description">bibliothèques pour les calcules scientifiques</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="database brown icon"></i>
                                    <div class="content">
                                        <div class="header">Base de données</div>
                                        <div class="description">apprentissage sur les bases SQL</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="microchip brown icon"></i>
                                    <div class="content">
                                        <div class="header">Système logique combinatoire et séquentiel</div>
                                        <div class="description">étude des différents systèmes logiques binaires</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="graduation cap brown icon"></i>
                        <div class="content">
                            <div class="header">Première année</div>
                            <div class="list">
                                <div class="item">
                                    <i class="globe brown icon"></i>
                                    <div class="content">
                                        <div class="header">Informatique et internet</div>
                                        <div class="description">intiation HTML, CSS et JavaScript</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="python brown icon"></i>
                                    <div class="content">
                                        <div class="header">Introduction à la programmation</div>
                                        <div class="description">premiers cours sur la programmation</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</div>
