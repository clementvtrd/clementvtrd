<section class="ui container">
    <article class="ui container segment">
        <p class="ui olive ribbon label">Avril - Juin 2020</p>
        <div class="ui top right attached label blue">Stage</div>
        <h2>Dupont d'Isigny - Carentan (50)</h2>
        <section class="ui container grid">
            <div class="eight wide column">
                <h3>Développeur web</h3>
                <p>
                    Développement d'une application web permettant de visualiser les indicateurs de production et de
                    maintenance de l'entreprise.
                </p>
                <p>
                    L'objectif était de clarifier les informations fournies par la GMAO utilisée. Celle-ci dispense les
                    informations dans un format brut difficilement assimilable par l'utilisateur.
                </p>
                <p>
                    Pour simplifier cette lecture, l'application devait récupérer les données désirées dans la base de
                    données de la GMAO afin des les afficher sous formes de graphiques adaptés.
                </p>
            </div>
            <div class="eight wide column">
                <h3>Technologies utilisées</h3>
                <div class="ui list">
                    <div class="item">
                        <i class="html5 large brown icon"></i>
                        <div class="content">
                            <p class="header">Client web HTML respectant les normes W3C</p>
                            <p class="description">généré avec le template Twig de Symfony</p>
                        </div>
                    </div>
                    <div class="item">
                        <i class="css3 large brown icon"></i>
                        <div class="content">
                            <p class="header">Utilisation du framework Bootstrap 3</p>
                            <p class="description">personnalisé pour adapter les différents vues de l'application</p>
                        </div>
                    </div>
                    <div class="item">
                        <i class="js square large brown icon"></i>
                        <div class="content">
                            <p class="header">Outils de visualisation de données : ChartJS</p>
                            <p class="description">alternative simple à D3.js pour généré des graphiques à barre</p>
                        </div>
                    </div>
                    <div class="item">
                        <i class="php large brown icon"></i>
                        <div class="content">
                            <p class="header">Application serveur homemade réalisée avec PHP 7</p>
                            <p class="description">hébergée sous IIS pour accéder aux données de la GMAO utilisée</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
</section>
