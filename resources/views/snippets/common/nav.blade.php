<nav class="ui olive three item top attached inverted menu">
    <a class="item{{ request()->routeIs('welcome') ? ' active' : '' }}" href="{{ route('welcome') }}">Accueil</a>
    <a class="item{{ request()->routeIs('education') ? ' active' : '' }}" href="{{ route('education') }}">Formation</a>
    <a class="item{{ request()->routeIs('projects') ? ' active' : '' }}" href="{{ route('projects') }}">Projets</a>
</nav>
