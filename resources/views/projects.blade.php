@extends('layout')
@section('main')
    <h1>Mes projets</h1>
    <section class="ui container grid">
        <div class="row">
            <article class="seven wide column ui container segment">
                <h3>Snowball</h3>
                <p>Développement d'une application VueJS basé sur le jeu <i>A good snowman is hard to build</i>.</p>
                <p>
                    L'objectif est de stocker l'état du jeu dans un <i>triple store</i> et afficher l'état courant
                    avec un client VueJS.
                </p>
                <div class="ui list">
                    <div class="item">
                        <i class="server large brown icon"></i>
                        <div class="content">
                            <p class="header">Stack technique</p>
                            <div class="list">
                                <div class="item">
                                    <i class="vuejs large green icon"></i>
                                    <div class="content">
                                        <p class="header">VueJS</p>
                                        <p class="description">client HTML réactif</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="database large brown icon"></i>
                                    <div class="content">
                                        <p class="header">Stardog</p>
                                        <p class="description"><i>triple store</i> pour gérer l'ontologie du jeu</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="gitlab large orange icon"></i>
                                    <div class="content">
                                        <p class="header">
                                            <a href="https://gitlab.com/confrerie-du-pouce/snowball">
                                                Dépôt du code
                                            </a>
                                        </p>
                                        <div class="description">
                                            <p>
                                                nécessite une license stardog pour gérer l'ontologie
                                            </p><small>(30 jours gratuits)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="seven wide column ui container segment">
                <h3>Nuremberg</h3>
                <p>Développement d'une application web pour rechercher du contenu dans la transcription annotée du
                procès de Nuremberg au format XML.</p>
                <p>L'objectif est de reproduire et améliorer les fonctionnalités d'un
                    <a href="https://www.unicaen.fr/recherche/mrsh/crdfed/nuremberg/accueil">
                        site existant
                    </a>.
                </p>
                <div class="ui list">
                    <div class="item">
                        <i class="database large brown icon"></i>
                        <div class="content">
                            <p class="header">Stack technique</p>
                            <div class="list">
                                <div class="item">
                                    <i class="laravel red icon"></i>
                                    <div class="content">
                                        <p class="header">Laravel</p>
                                        <p class="description">
                                            backend réalisé pour récupérer de données et faire la mise en page
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="database large brown icon"></i>
                                    <div class="content">
                                        <p class="header">BaseX</p>
                                        <p class="description">
                                            serveur BaseX pour indexer le corpus XML
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="file code large brown icon"></i>
                                    <div class="content">
                                        <p class="header">XQuery</p>
                                        <p class="description">
                                            requête XQuery à BaseX pour rechercher le contenu demandé
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="gitlab large orange icon"></i>
                                    <div class="content">
                                        <p class="header">
                                            <a href="https://gitlab.com/confrerie-du-pouce/nuremberg">
                                                Dépôt du code
                                            </a>
                                        </p>
                                        <p class="description">nécessite un serveur PHP 7.4+</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row">
            <article class="seven wide column ui container segment">
                <h3>Selenium Webtesting</h3>
                <p>Projet annuel proposé lors de la première année du master informatique.</p>
                <p>L'objectif était de réaliser une application permettant de tester une multitude de site (via URL ou
                dossier) avec une même série de test.</p>
                <p>Lors de l'avancé du projet, de multiple problème ont été rencontrés aboutissant à la conclusion
                que Selenium n'était pas adapté pour réaliser cette tâche.</p>
                <div id="selenium" class="ui olive progress" data-value="17.5" data-total="20">
                    <div class="bar">
                        <div class="progress"></div>
                    </div>
                    <div class="label">Projet apprécié pour les tests effectués avec l'API Selenium</div>
                </div>
            </article>
        </div>
    </section>
@endsection
