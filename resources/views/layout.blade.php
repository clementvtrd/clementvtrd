<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title>Clément Vétillard</title>
    <link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>
<body>
@include('snippets.common.nav')
<main>
    @yield('main')
</main>
<footer class="ui container grid">
    <div class="row"></div>
    <div class="row">
        <div class="three wide column ui">
            <a href="{{ asset('files/clement-vetillard_curriculum.pdf') }}">
                <i class="file pdf outline red large icon"></i>
            </a>
        </div>
        <div class="three wide column ui">
            <a href="https://www.linkedin.com/in/clementvtrd/">
                <i class="linkedin large icon"></i>
            </a>
        </div>
        <div class="three wide column ui">
            <a href="https://gitlab.com/clementvtrd">
                <i class="gitlab large orange icon"></i>
            </a>
        </div>
        <div class="three wide column ui">
            <a href="https://facebook.com/clementvtrd">
                <i class="facebook large blue icon"></i>
            </a>
        </div>
        <div class="three wide column ui">
            <a href="https://twitter.com/clementvtrd">
                <i class="twitter large icon"></i>
            </a>
        </div>
    </div>
</footer>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/semantic.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
