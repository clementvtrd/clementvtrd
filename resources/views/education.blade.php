@extends('layout')
@section('main')
    <section class="ui container">
        <h1>Mes diplômes</h1>
        @include('snippets.education.diplomes')
    </section>
    <section class="ui container">
        <h1>Mon expérience professionnelle</h1>
        @include('snippets.education.experience')
    </section>
@endsection
