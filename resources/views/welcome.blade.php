@extends('layout')
@section('main')
    <header class="ui grid container segment">
        <div class="three wide column">
            <img class="ui medium circular image" src="{{ asset('pic/profil.jpg') }}" alt="" />
        </div>
        <div class="thirteen wide column">
            <blockquote>
                <h3>
                    Bonjour et bienvenue sur mon portfolio numérique !
                </h3>
                <p>Je m'appelle Clément Vétillard. Je suis étudiant
                    en informatique à <a href="https://www.unicaen.fr">Unicaen</a>, l'université de Caen en Normandie.
                </p>
                <p>
                    Actuellement en deuxième année du <a href="https://www.info.unicaen.fr/index.php/master/info/idc">
                    Master Internet, Données et Connaissances</a>, je recherche un stage de fin d'étude en
                    entreprise. Le stage, d'une durée de 4 à 6 mois, se déroulera entre le 1er Mars et le 31 Août 2021.
                </p>
                <p>
                    Le stage idéal me permettrait d'exploiter mes connaissances dans le domaine du big data et d'en
                    acquérir de nouvelles dans une équipe bienveillante. N'hésitez pas à aller voir
                    <a href="{{ route('education') }}">mon parcours</a> pour avoir plus de détails sur mes capacités.
                </p>
                <p>
                    Si mon profil pourrait intéresser votre entreprise, n'hésitez pas à me contacter.
                </p>
            </blockquote>
        </div>
    </header>
    <section class="ui two column grid container">
        <article class="column ui container">
            <h2>Contact</h2>
            <p class="ui container segment">
                Pour me contacter, n'hésitez pas à m'envoyer un email à
                <a href="mailto:contact@clementvtrd.fr">
                    contact@clementvtrd.fr
                </a>
                ou bien via les réseaux sociaux (disponibles en bas de page). Je vous recontacterai en retour dans les
                48 heures.
            </p>
            <h2>Localisation</h2>
            <p class="ui container segment">
                Je réside actuellement sur Caen. Cependant, dans le cadre de mon stage je n'hésiterais pas à déménager
                afin de me rapprocher de l'entreprise d'accueil. Possédant ma propre voiture, je suis donc disponible
                pour un stage dans toute la France.
            </p>
        </article>
        <article class="column ui container">
            <h2>Mes objectifs</h2>
            <p class="ui container segment">
                En tant que développeur, j'ai un certain attrait pour tout ce qui touche au backend des applications
                web. En entreprise, j'aspire à développer des applications en utilisant la méthode DevOps.
                L'intégration et le développement continus permettent le déploiement et la maintenance des applications
                en temps réel.
            </p>
            <p class="ui container segment">
                À l'avenir, j'espère avoir l'occasion de prouver mes capacités à gérer un projet allant de la conception
                au déploiement. Ma formation m'a permis d'acquérir les bases de gestion de projet, des bases que je
                souhaite approfondir.
            </p>
            <p class="ui container segment">
                À titre personnel, je rêve de pouvoir visiter de fond en comble le Japon. La culture m'a toujours
                fasciné depuis ma jeunesse. Tant leur architecture que leur tradition. J'envisage de partir là-bas
                à plusieurs occasions pour visiter les multiples facettes du pays du Soleil Levant. À cette fin, j'apprends
                le japonais, écris et oral, en autodidacte.
            </p>
        </article>
    </section>
@endsection
